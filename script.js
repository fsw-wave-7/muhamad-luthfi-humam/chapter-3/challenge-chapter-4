class Card {
    constructor(cardID) {
        this.cardID = cardID
        this.options = [ 
            {
                name: 'batu',
                url: 'assets/batu.png'
            },
            {
                name: 'gunting',
                url: 'assets/gunting.png'
            },
            {
                name: 'kertas',
                url: 'assets/kertas.png'
            }
        ]
    }

    getCard() {
        const t = this
        return this.options.forEach((opt) => {
            const img = document.createElement('img')
            img.src = opt.url
            img.className = ''
            img.id = `card-${opt.name}`
            document.getElementById('cards').append(img)
        })
    }

    getOptions() {
        return this.options;
    }
    
    choosePic(suwit) {
        document.getElementsByClassName('choose-result')[0].innerHTML = `player choose ${suwit}`
    }
}

class RandomCard extends Card {
    constructor(cardId) {
        super(cardId)
    }

    random() {
        let com = Math.floor(Math.random() * Math.floor(3))
        const options = super.getOptions()
        return options[com]
    }
}

const card = new Card('cards')
card.getCard()
const cardComp = new RandomCard('cards-comp')
cardComp.getCard()

// rules
function getResult(player, comp) {
    let text = ''
    if(player.name == comp.name) {
        text = `DRAW`
    }else if(player.name == `batu` && comp.name == `gunting`) {
        text = `PLAYER WIN`
    }else if(player.name == `batu` && comp.name == `kertas`) {
        text = `COM WIN`
    }else if(player.name == `gunting` && comp.name == `batu`) {
        text = `COM WIN`
    }else if(player.name == `gunting` && comp.name == `kertas`) {
        text = `PLAYER WIN`
    }else if(player.name == `kertas` && comp.name == `batu`) {
        text = `PLAYER WIN`
    }else if(player.name == `kertas` && comp.name == `gunting`) {
        text = `COM WIN`
    }

    document.getElementsByClassName('result')[0].innerHTML = text
    console.log(player, comp)
}

console.log(getResult)

const options = card.getOptions()
options.forEach((opt) => {
    document.getElementById(`card-${opt.name}`).addEventListener('click', () => {
        const player = opt
        const comp = cardComp.random()
        getResult(player, comp)
    })
})